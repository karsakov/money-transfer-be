package su.ssau.transport;

import su.ssau.model.invariants.CurrencyCode;
import su.ssau.model.invariants.TransferCommandTagNames;

import javax.json.bind.annotation.JsonbProperty;
import java.io.Serializable;

public class TransferBetweenAccountsCommand implements Serializable {

    @JsonbProperty
    private String sourceAccountNumber;
    @JsonbProperty
    private String transferAmount;
    @JsonbProperty
    private String currencyCode;
    @JsonbProperty
    private String destAccountNumber;

    public TransferBetweenAccountsCommand() {
        sourceAccountNumber = "";
        transferAmount = "0";
        currencyCode = CurrencyCode.RUB.name();
        destAccountNumber = "";
    }

    public TransferBetweenAccountsCommand(String sourceAccountNumber, String transferAmount, String currencyCode, String destAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
        this.transferAmount = transferAmount;
        this.currencyCode = currencyCode;
        this.destAccountNumber = destAccountNumber;
    }

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public void setSourceAccountNumber(String sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public String getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(String transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getDestAccountNumber() {
        return destAccountNumber;
    }

    public void setDestAccountNumber(String destAccountNumber) {
        this.destAccountNumber = destAccountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof TransferBetweenAccountsCommand)) return false;

        TransferBetweenAccountsCommand command = (TransferBetweenAccountsCommand) o;
        return command.getSourceAccountNumber().equals(sourceAccountNumber)
                && command.getTransferAmount().equals(transferAmount)
                && command.getCurrencyCode().equals(currencyCode)
                && command.getDestAccountNumber().equals(destAccountNumber);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash = hash * 31 + sourceAccountNumber.hashCode();
        hash = hash * 31 + transferAmount.hashCode();
        hash = hash * 31 + currencyCode.hashCode();
        hash = hash * 31 + destAccountNumber.hashCode();
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("{\"TransferBetweenAccountsCommand\":{");
        builder.append("\"").append(TransferCommandTagNames.SOURCE_ACCOUNT_NUMBER_TAG).append("\":\"").append(sourceAccountNumber).append("\",");
        builder.append("\"").append(TransferCommandTagNames.CURRENCY_CODE_TAG).append("\":\"").append(currencyCode).append("\",");
        builder.append("\"").append(TransferCommandTagNames.DEST_ACCOUNT_NUMBER_TAG).append("\":\"").append(destAccountNumber).append("\",");
        builder.append("\"").append(TransferCommandTagNames.TRANSFER_AMOUNT_TAG).append("\":\"").append(transferAmount).append("\"");
        builder.append("}}");

        return builder.toString();
    }

}
