package su.ssau;

import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.h2.tools.RunScript;
import su.ssau.model.utils.TransferConnectionFactory;

import java.io.StringReader;
import java.net.URI;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {

    public static final String CREATE_ACCOUNTS_TABLE_QUERY = "create table if not exists accounts (account_number varchar primary key, currency_code varchar(3));";
    public static final String CREATE_OPERATIONS_TABLE_QUERY =
            "create table if not exists operations (account_number varchar, transfer_amount decimal, foreign key (account_number) references accounts(account_number));";

    public static void main(String[] args) {
        initDB();

        URI uri = URI.create("http://localhost:8080/transferApp");
        ResourceConfig resource = new ResourceConfig().packages("su.ssau.api");
        HttpServer server = JdkHttpServerFactory.createHttpServer(uri, resource);
    }

    public static void initDB() {
        Connection conn = TransferConnectionFactory.getConnection();

        StringBuilder script = new StringBuilder(CREATE_ACCOUNTS_TABLE_QUERY);
        script.append(CREATE_OPERATIONS_TABLE_QUERY);

        try {
            RunScript.execute(conn, new StringReader(script.toString()));
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            TransferConnectionFactory.closeConnection(conn);
        }
    }

}
