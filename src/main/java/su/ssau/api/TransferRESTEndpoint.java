package su.ssau.api;

import su.ssau.model.api.Account;
import su.ssau.model.utils.AccountBuilder;
import su.ssau.model.utils.CurrencyConverter;
import su.ssau.transport.TransferBetweenAccountsCommand;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("transfer")
public class TransferRESTEndpoint {

    @POST
    @Path("betweenAccounts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfer(TransferBetweenAccountsCommand transferCommand) {
        Account donor = AccountBuilder.findAccount(transferCommand.getSourceAccountNumber());
        Account acceptor = AccountBuilder.findAccount(transferCommand.getDestAccountNumber());
        BigDecimal transferAmount = new BigDecimal(transferCommand.getTransferAmount());
        BigDecimal transferredMoney = CurrencyConverter.convert(donor.getCurrencyCode(), transferAmount, acceptor.getCurrencyCode());
        donor.withdrawMoney(transferAmount);
        acceptor.depositMoney(transferredMoney);
        return Response.ok().build();
    }

}
