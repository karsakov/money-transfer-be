package su.ssau.model.invariants;

public enum CurrencyCode {
    USD("840"),
    EUR("978"),
    GBP("826"),
    CHF("756"),
    CNY("156"),
    JPY("392"),
    RUB("643")
    ;

    private String isoCode;

    CurrencyCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

}
