package su.ssau.model.invariants;

public final class TransferCommandTagNames {

    public static final String SOURCE_ACCOUNT_NUMBER_TAG = "sourceAccountNumber";
    public static final String TRANSFER_AMOUNT_TAG = "transferAmount";
    public static final String CURRENCY_CODE_TAG = "currencyCode";
    public static final String DEST_ACCOUNT_NUMBER_TAG = "destAccountNumber";

    private TransferCommandTagNames() {
    }
}
