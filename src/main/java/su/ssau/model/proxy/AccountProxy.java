package su.ssau.model.proxy;

import su.ssau.model.api.Account;
import su.ssau.model.api.Detached;
import su.ssau.model.invariants.CurrencyCode;
import su.ssau.model.utils.AccountSyncService;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountProxy implements Account, Detached {

    private static final int SYNC_CACHE_LIMIT = 25;

    private String accountNumber;
    private CurrencyCode currency;
    private BigDecimal balance = BigDecimal.ZERO;

    private List<BigDecimal> commandsToSync = new ArrayList<>(SYNC_CACHE_LIMIT); //Non-synced history of operations

    public AccountProxy(String accountNumber, CurrencyCode currencyCode) {
        this.accountNumber = accountNumber;
        this.currency = currencyCode;
    }

    public AccountProxy(String accountNumber, CurrencyCode currencyCode, BigDecimal balance) {
        this(accountNumber, currencyCode);
        this.balance = balance;
    }

    public void setCurrencyCode(CurrencyCode currency) {
        this.currency = currency;
    }

    public CurrencyCode getCurrencyCode() {
        return currency;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    synchronized public void depositMoney(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(NEGATIVE_MONEY_VALUE_ERROR_MESSAGE);
        }
        this.balance.add(money);
        commandsToSync.add(money);
        if (commandsToSync.size() >= SYNC_CACHE_LIMIT) syncWithStore();
    }

    synchronized public void withdrawMoney(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(NEGATIVE_MONEY_VALUE_ERROR_MESSAGE);
        }
        this.balance.subtract(money);
        commandsToSync.add(money.negate());
        if (commandsToSync.size() >= SYNC_CACHE_LIMIT) syncWithStore();
    }

    @Override
    synchronized public boolean syncWithStore() {
        boolean isSyncOK = false;
        try {
            isSyncOK = AccountSyncService.syncAccount(accountNumber, commandsToSync);
            if (isSyncOK) commandsToSync.clear();
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if (!isSyncOK) {
                Logger.getGlobal().log(Level.SEVERE, "Account #" + this.accountNumber + " doesn't exist! Synchronization failed!");
            }
            return isSyncOK;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        syncWithStore();
    }

}