package su.ssau.model.dao;

import su.ssau.model.api.Account;
import su.ssau.model.invariants.CurrencyCode;
import su.ssau.model.utils.ResourceDisposer;
import su.ssau.model.utils.TransferConnectionFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountDao implements Account {

    public static final String WRITE_OPERATION_QUERY = "insert into operations(account_number, transfer_amount) values (?, ?)";

    private static final String GET_BALANCE_QUERY = "select SUM(transfer_amount) from operations where account_number = ?";
    private static final String INIT_ACCOUNT_QUERY = "select * from accounts where account_number = ?";

    private String accountNumber;
    private String currencyCode;

    public AccountDao(String accountNumber) {
        Connection conn = TransferConnectionFactory.getConnection();
        PreparedStatement query = null;
        ResultSet rs = null;

        try {
            query = conn.prepareStatement(INIT_ACCOUNT_QUERY);
            query.setString(1, accountNumber);
            rs = query.executeQuery();
            if (rs.first()) {
                this.currencyCode = rs.getString("currency_code");
            }
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            ResourceDisposer.closeResource(rs);
            ResourceDisposer.closeResource(query);
            TransferConnectionFactory.closeConnection(conn);
        }

        this.accountNumber = accountNumber;
    }

    @Override
    public String getAccountNumber() {
        return accountNumber;
    }

    @Override
    public BigDecimal getBalance() {
        Connection conn = TransferConnectionFactory.getConnection();
        PreparedStatement query = null;
        ResultSet rs = null;
        BigDecimal balance = BigDecimal.ZERO;
        try {
            query = conn.prepareStatement(GET_BALANCE_QUERY);
            query.setString(1, accountNumber);
            rs = query.executeQuery();
            if (rs.first()) {
                balance = rs.getBigDecimal(1);
            }
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            ResourceDisposer.closeResource(rs);
            ResourceDisposer.closeResource(query);
            TransferConnectionFactory.closeConnection(conn);
        }
        return balance != null ? balance : BigDecimal.ZERO;
    }

    @Override
    public CurrencyCode getCurrencyCode() {
        return CurrencyCode.valueOf(currencyCode);
    }

    @Override
    public void depositMoney(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(NEGATIVE_MONEY_VALUE_ERROR_MESSAGE);
        }
        persistChangeBalance(money);
    }

    @Override
    public void withdrawMoney(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(NEGATIVE_MONEY_VALUE_ERROR_MESSAGE);
        }
        persistChangeBalance(money.negate());
    }

    private void persistChangeBalance(BigDecimal money) {
        Connection conn = TransferConnectionFactory.getConnection();
        PreparedStatement query = null;
        try {
            query = conn.prepareStatement(WRITE_OPERATION_QUERY);
            query.setString(1, accountNumber);
            query.setBigDecimal(2, money);
            query.execute();
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            ResourceDisposer.closeResource(query);
            TransferConnectionFactory.closeConnection(conn);
        }
    }

}
