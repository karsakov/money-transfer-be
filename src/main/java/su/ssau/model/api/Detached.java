package su.ssau.model.api;

public interface Detached {

    boolean syncWithStore();

}
