package su.ssau.model.api;

import su.ssau.model.invariants.CurrencyCode;

import java.math.BigDecimal;

public interface Account {

    static final String NEGATIVE_MONEY_VALUE_ERROR_MESSAGE = "The money value should not be negative.";

    String getAccountNumber();

    BigDecimal getBalance();

    CurrencyCode getCurrencyCode();

    void depositMoney(BigDecimal money);

    void withdrawMoney(BigDecimal money);

}
