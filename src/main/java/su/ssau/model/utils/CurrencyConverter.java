package su.ssau.model.utils;

import su.ssau.model.invariants.CurrencyCode;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.net.URI;

public class CurrencyConverter {

    private static final String EXCHANGE_INFO_API_SOURCE = "https://free.currencyconverterapi.com/api/v6/convert";

    public static BigDecimal convert(CurrencyCode sourceCurrencyCode, BigDecimal sumOfIncome, CurrencyCode destCurrencyCode) {
        StringBuilder exchangePair = new StringBuilder();
        exchangePair.append(sourceCurrencyCode.name()).append("_").append(destCurrencyCode.name());

        URI uri = URI.create(EXCHANGE_INFO_API_SOURCE);
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(uri);
        Response resp = target
                .queryParam("q", exchangePair)
                .queryParam("compact", "y")
                .request(MediaType.APPLICATION_JSON_TYPE).get();
        String response = resp.readEntity(String.class);
        int valueBeginIndex = response.lastIndexOf(":") + 1;
        int valueEndIndex = response.indexOf("}", valueBeginIndex);
        double relation = Double.valueOf(response.substring(valueBeginIndex, valueEndIndex));
        return sumOfIncome.multiply(BigDecimal.valueOf(relation));
    }

}
