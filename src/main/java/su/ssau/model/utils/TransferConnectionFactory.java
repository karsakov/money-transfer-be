package su.ssau.model.utils;

import su.ssau.transport.TransferBetweenAccountsCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TransferConnectionFactory {

    private static final String PROPERTY_KEY_VALUE_DELIMITER = "=";

    private static final String CONFIG_FILE_URL = "conn.properties";

    enum ConnectionProperties {
        USER_NAME("db.user.name"),
        USER_PASS("db.password"),
        DRIVER("db.driver"),
        CONNECTION_STRING("db.connection");

        private String propertyName;

        ConnectionProperties(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyName() {
            return propertyName;
        }
    }

    private static String DB_USER_NAME;
    private static String DB_PASSWORD;
    private static String DB_DRIVER;
    private static String DB_CONNECTION_STRING;

    static {
        ClassLoader loader = TransferBetweenAccountsCommand.class.getClassLoader();
        BufferedReader reader = new BufferedReader(new InputStreamReader(loader.getResourceAsStream(CONFIG_FILE_URL)));
        List<String> configs = reader.lines().collect(Collectors.toList());

        for (String configItem : configs) {
            int delimiterPos = configItem.indexOf(PROPERTY_KEY_VALUE_DELIMITER);
            if (delimiterPos > -1) {
                String key = configItem.substring(0, delimiterPos);
                String value = configItem.substring(delimiterPos + 1);
                if (ConnectionProperties.USER_NAME.getPropertyName().equals(key)) {
                    DB_USER_NAME = value;
                } else if (ConnectionProperties.USER_PASS.getPropertyName().equals(key)) {
                    DB_PASSWORD = value;
                } else if (ConnectionProperties.DRIVER.getPropertyName().equals(key)) {
                    DB_DRIVER = value;
                } else if (ConnectionProperties.CONNECTION_STRING.getPropertyName().equals(key)) {
                    DB_CONNECTION_STRING = value;
                }
            }
        }
    }

    public static Connection getConnection() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_CONNECTION_STRING, DB_USER_NAME, DB_PASSWORD);
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        }

        return conn;
    }

    public static void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

}
