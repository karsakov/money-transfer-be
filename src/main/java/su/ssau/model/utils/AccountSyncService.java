package su.ssau.model.utils;

import su.ssau.model.dao.AccountDao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountSyncService {

    private static final String FIND_ACCOUNT_QUERY = "select COUNT(*) from accounts where account_number = ?";

    synchronized public static boolean syncAccount(String accountNumber, List<BigDecimal> operations) throws SQLException {
        Connection conn = TransferConnectionFactory.getConnection();
        PreparedStatement query = null;

        if (!findAccount(accountNumber)) return false;

        try {
            conn.setAutoCommit(false);
            query = conn.prepareStatement(AccountDao.WRITE_OPERATION_QUERY);
            for (BigDecimal operation : operations) {
                query.setString(1, accountNumber);
                query.setBigDecimal(2, operation);

                query.addBatch();
            }
            query.execute();
            conn.commit();
        } catch (SQLException e) {
            conn.rollback();
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            if (query != null) {
                query.close();
            }
            TransferConnectionFactory.closeConnection(conn);
        }
        return true;
    }

    private static boolean findAccount(String accountNumber) {
        Connection conn = TransferConnectionFactory.getConnection();
        PreparedStatement query = null;
        ResultSet rs = null;

        boolean isFound = false;

        try {
            query = conn.prepareStatement(FIND_ACCOUNT_QUERY);
            query.setString(1, accountNumber);
            rs = query.executeQuery();
            if (rs.first()) {
                isFound = rs.getInt(1) != 0;
            }
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            ResourceDisposer.closeResource(rs);
            ResourceDisposer.closeResource(query);
            TransferConnectionFactory.closeConnection(conn);
            return isFound;
        }
    }

}
