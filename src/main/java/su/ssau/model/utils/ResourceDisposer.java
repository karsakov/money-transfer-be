package su.ssau.model.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ResourceDisposer {

    public static void closeResource(AutoCloseable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (Exception e) {
                Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

}
