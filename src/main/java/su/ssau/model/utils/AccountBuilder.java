package su.ssau.model.utils;

import su.ssau.model.api.Account;
import su.ssau.model.dao.AccountDao;
import su.ssau.model.proxy.AccountProxy;

import java.lang.ref.WeakReference;
import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AccountBuilder {

    private static final Map<String, WeakReference<Account>> accountsCache = new ConcurrentHashMap<>();

    public static Account findAccount(String accountNumber) {
        WeakReference<Account> cachedItem = accountsCache.get(accountNumber);
        if (cachedItem != null && cachedItem.get() != null) {
            return cachedItem.get();
        } else {
            Account account = new AccountDao(accountNumber);
            accountsCache.put(accountNumber, new WeakReference<>(account));
            return account;
        }
    }

    public static Account getAccount(String accountNumber, boolean isLightweight) {
        Account foundAccount = findAccount(accountNumber);

        return isLightweight ? new AccountProxy(accountNumber, foundAccount.getCurrencyCode(), foundAccount.getBalance()) : foundAccount;
    }

    public static String generateAccountNumber() {
        SecureRandom rnd = new SecureRandom();
        long val = rnd.nextLong();
        return String.valueOf(val > 0 ? val : -val);
    }

}
