package su.ssau.api;

import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import su.ssau.Application;
import su.ssau.model.api.Account;
import su.ssau.model.invariants.CurrencyCode;
import su.ssau.model.proxy.AccountProxy;
import su.ssau.model.utils.AccountBuilder;
import su.ssau.model.utils.CurrencyConverter;
import su.ssau.model.utils.TransferConnectionFactory;
import su.ssau.transport.TransferBetweenAccountsCommand;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.StringReader;
import java.math.BigDecimal;
import java.net.URI;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class TransferRESTEndpointTest {

    private static HttpServer server;
    private static WebTarget target;
    private static List<String> accountNumbers = new ArrayList<>(3);

    @BeforeAll
    static void setUp() {
        URI uri = URI.create("http://localhost:8080/transferApp");
        ResourceConfig resource = new ResourceConfig().packages("su.ssau.api");
        server = JdkHttpServerFactory.createHttpServer(uri, resource);

        Client client = ClientBuilder.newClient();

        target = client.target(uri).path("transfer").path("betweenAccounts");

        initDB();
    }

    @AfterAll
    static void tearDown() {
        server.stop(0);
    }

    @Test
    void transferFromAccountToAccount_sameCurrencyTest() {
        Account donor = AccountBuilder.findAccount(accountNumbers.get(0));
        donor.depositMoney(BigDecimal.valueOf(100));
        Account acceptor = AccountBuilder.findAccount(accountNumbers.get(1));
        BigDecimal oldDonorBalance = donor.getBalance();
        BigDecimal oldAcceptorBalance = acceptor.getBalance();

        TransferBetweenAccountsCommand command = new TransferBetweenAccountsCommand(donor.getAccountNumber(), "20", donor.getCurrencyCode().name(), acceptor.getAccountNumber());

        Response response = target.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(command));

        assertTrue(response.getStatus() == Response.Status.OK.getStatusCode()
                && donor.getBalance().compareTo(oldDonorBalance.subtract(BigDecimal.valueOf(20))) == 0
                && acceptor.getBalance().compareTo(oldAcceptorBalance.add(BigDecimal.valueOf(20))) == 0
        );
    }

    @Test
    void transferFromAccountToAccount_differentCurrencyTest() {
        Account donor = AccountBuilder.findAccount(accountNumbers.get(2));
        donor.depositMoney(BigDecimal.valueOf(100));
        Account acceptor = AccountBuilder.findAccount(accountNumbers.get(0));
        BigDecimal oldDonorBalance = donor.getBalance();
        BigDecimal oldAcceptorBalance = acceptor.getBalance();

        TransferBetweenAccountsCommand command = new TransferBetweenAccountsCommand(donor.getAccountNumber(), "20", donor.getCurrencyCode().name(), acceptor.getAccountNumber());

        Response response = target.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.json(command));
        BigDecimal addedSum = CurrencyConverter.convert(donor.getCurrencyCode(), BigDecimal.valueOf(20), acceptor.getCurrencyCode());

        assertTrue(response.getStatus() == Response.Status.OK.getStatusCode()
                && donor.getBalance().compareTo(oldDonorBalance.subtract(BigDecimal.valueOf(20))) == 0
                && acceptor.getBalance().compareTo(oldAcceptorBalance.add(addedSum)) == 0
        );
    }

    private static void initDB() {
        Connection conn = TransferConnectionFactory.getConnection();

        StringBuilder script = new StringBuilder(Application.CREATE_ACCOUNTS_TABLE_QUERY);
        script.append(Application.CREATE_OPERATIONS_TABLE_QUERY);

        for (int i = 0; i < 3; ++i) {
            String accountNumber = AccountBuilder.generateAccountNumber();
            accountNumbers.add(accountNumber);
            String currencyCode = i > 1 ? CurrencyCode.values()[i].name() : CurrencyCode.RUB.name(); //Just need some different currencies for tests.
            script.append("insert into accounts(account_number, currency_code) values ('").append(accountNumber).append("', '").append(currencyCode).append("');");
        }

        try {
            RunScript.execute(conn, new StringReader(script.toString()));
        } catch (SQLException e) {
            Logger.getGlobal().log(Level.SEVERE, e.getMessage(), e);
        } finally {
            TransferConnectionFactory.closeConnection(conn);
        }
    }

}